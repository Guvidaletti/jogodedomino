import java.util.Random;
/**
 * Escreva a descrição da classe Baralho aqui.
 * 
 * @author Gustavo Lodi Vidaletti 
 * @version 2.0
 */
public class Baralho
{
    private int tamanho;
    private Peca[] pecas;
    public Baralho()
    {
        tamanho=0;
        pecas = new Peca[14];
    }
    
    public void setTamanho(int tamanho){this.tamanho = tamanho;}
    
    public void addPeca(Peca aPeca)
    {
        pecas[tamanho] = aPeca;
        tamanho++;
    }
    
    public Peca buscarPeca(int num){return pecas[num];}
    
    public Peca Comprar()
    {
        Random gerador = new Random();
        Peca aPeca; 
        int numero = gerador.nextInt(tamanho);
        aPeca=pecas[numero];
        for(int i=numero; i < tamanho-1 ; i++)
            {
                pecas[i] = pecas[i+1];
            }
        tamanho--;
        return aPeca;
    }
    
    public int getTamanho(){return tamanho;}
    
    public boolean Vazio()
    {
        if (tamanho > 0) return false;
        return true;
    }
}