public class Player {
    private int id,mao=0;
    private String nome;
    private Peca[] Mao;
    
    public Player(int id,String nome)
    {
        this.id=id; 
        this.nome=nome;
        mao=0;
        Mao = new Peca[21];
    }
    
    public void setNome(String nome){this.nome=nome;}
    
    public void setMao(int n){mao=n;}
    
    public void addPeca(Peca nova)
    {
        Mao[mao] = nova;
        mao++;
    }
    
    public Peca buscarPeca(int num){return Mao[num];}
    
    public void excluir(int num)
    {
        //Peca aPeca = buscarPeca(num);
        for(int i=num; i<mao-1;i++)
        {
            Mao[i] = Mao[i+1];
        }
        mao--;
    }
    
    public int getId(){return id;}
    
    public int getMao(){return mao;}
    
    public String getNome(){return nome.toUpperCase();}
    
    public Peca[] getPecas(){return Mao;}
    
    public int SomarPontos()
    {   
        Peca aPeca;
        int soma=0;
        for(int i=0;i<mao;i++)
        {
            aPeca = Mao[i];
            soma = soma + aPeca.getL1() + aPeca.getL2();
        }
        return soma;
    }
    
    public String toString()
    {
        String msg = "MAO DO(A) ";
        msg = msg + getNome() +":\n";
        for ( int i=0; i<mao ; i++)
        {
            if(i!=0 && i%7==0) msg = msg + "\n";
            msg = msg + "Peça"+ (i+1)+ Mao[i].toString() + " ";
            }
            return msg;
    }
}