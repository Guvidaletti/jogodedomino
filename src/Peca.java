
/**
 * Escreva a descrição da classe Peca aqui.
 * 
 * @author Gustavo Lodi Vidaletti
 * @version 2.0
 */
public class Peca
{
    private int lado1,lado2;
    /**
     * Método Construtor
     */
    public Peca(int lado1, int lado2)
    {
        this.lado1 = lado1;
        this.lado2 = lado2;
    }
    
    /**
     * getLado1
     */
    public int getL1() {return lado1;}
    
    /**
     * getLado2
     */
    public int getL2() {return lado2;}

    /**
     * Método para conferir se é Dobre
     * 6|6, 5|5, 4|4, 3|3, 2|2, 1|1 ou 0|0
     */
    public boolean ConfereDoble()
    {
        if(lado1 == lado2) return true;
        else return false;
    }
       
    /**
     * Método para poder encaixar a peça dos dois lados
     */
    public void inverterPeca()
    {
        int laranja;
        laranja = lado1;
        lado1 = lado2;
        lado2 = laranja;
    }
    
    /**
     * toString
     */
    public String toString()
    {
        String msg;
        msg = "[" + lado1 + "|" + lado2 + "]";
        return msg;
    }
}
