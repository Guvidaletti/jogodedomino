import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException; //Necessário em razão da exceção gerada quando o arquivo não é encontrado.
/**
 * Escreva a descrição da classe TesteJogo aqui.
 * 
 * @Gustavo Lodi Vidaletti 
 * @2.0
 */
public class oJogo
{
    public static void main(String args[])throws FileNotFoundException
    {
        Scanner in = new Scanner(System.in);
        Tabuleiro mesa = new Tabuleiro();
        System.out.print("\f");
        System.out.println("---JOGO DE DOMINÓ---");
        oJogo teste = new oJogo();
        teste.appMenu(mesa);
    }

    private void appMenu(Tabuleiro mesa)throws FileNotFoundException
    {
        Scanner in = new Scanner(System.in);
        int op=4,limite=0;
        do{
            boolean existe = mesa.SalvoExiste(4);
            System.out.println("----MENU USUARIO----");
            System.out.println("1 - Novo Jogo");

            if (existe){
                System.out.println("2 - Continuar jogo salvo");
                System.out.println("3 - Deletar jogo salvo");
                limite=3;
            }
            else limite=1;
            System.out.println("0 - Sair do jogo");
            do{
                try{
                    System.out.print("Informe a opção: ");
                    String sop = in.nextLine();
                    op = Integer.parseInt(sop);
                    if(op<0 || op>limite){System.out.println("Digite um valor entre 0 e "+ limite +"!");op=-1;}
                }catch(Exception erro){
                    System.out.println("Digite um valor válido!");
                    op=-1;
                }
            }while(op<0 || op>limite);
            switch(op){
                case 0: existe = mesa.SalvoExiste(4);
                        if(!existe) 
                        {
                            File origem = new File("JogosSalvos.txt");
                            origem.delete();
                        }
                System.out.print("Fechando o Aplicativo");
                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(500);
                                    }catch(Exception e){}
                                }
                System.exit(0);
                break;
                case 1: menuNovoJogo(mesa);
                    break;
                    
                case 2: if(existe){
                            System.out.println("--Continuar jogo salvo--");
                            System.out.println("----SLOTS DE MEMÓRIA----");
                            int op2=0;
                            try{
                                File origem = new File("JogosSalvos.txt");
                                Scanner arquivo = new Scanner(origem);
                                for(int i=0;i<3;i++)System.out.println(arquivo.nextLine());
                                arquivo.close();
                                System.out.println("0 - Voltar");
                            }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
                            do{
                                try{
                                    System.out.print("Digite a opção: ");
                                    String sop = in.nextLine();
                                    op2 = Integer.parseInt(sop);
                                    if(op2==0)break;
                                    if(mesa.SalvoExiste(op2)==false){System.out.println("Digite um Slot válido!");op2=-1;}
                                }catch(Exception erro){
                                    System.out.println("Digite um valor válido!");
                                    op2=-1;
                                }
                            }while(op2==-1);
                            if(op2!=0){
                                mesa.carregarJogo(op2);
                                System.out.print("Carregando Slot" + op2);
                                for(int i=0;i<3;i++)
                                    {
                                        try{
                                            System.out.print(".");
                                            Thread.sleep(800);
                                        }catch(Exception e){}
                                    }
                                System.out.print("\f");
                                menuPlayer(mesa,mesa.playersAtual(),mesa.playerAtual());
                                }
                                else System.out.print("\f");
                        }
                    break;
                    
                case 3:     System.out.println("--Apagar um jogo salvo--");
                            System.out.println("----SLOTS DE MEMÓRIA----");
                            int op2=0;
                            try{
                                File origem = new File("JogosSalvos.txt");
                                Scanner arquivo = new Scanner(origem);
                                for(int i=0;i<3;i++)System.out.println(arquivo.nextLine());
                                arquivo.close();
                                System.out.println("0 - Voltar");
                            }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
                            do{
                                try{
                                    System.out.print("Digite a opção: ");
                                    String sop = in.nextLine();
                                    op2 = Integer.parseInt(sop);
                                    if(op2==0) break;
                                    if(mesa.SalvoExiste(op2)==false){System.out.println("Digite um Slot válido!");op2=-1;}
                                }catch(Exception erro){
                                    System.out.println("Digite um valor válido!");
                                    op2=-1;
                                }
                            }while(op2==-1);
                            if(op2!=0){
                                mesa.DeletarSlotSalvo(op2);
                                System.out.print("Apagando Slot" + op2);
                                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(600);
                                    }catch(Exception e){}
                                }
                            }
                                System.out.print("\f");
                                appMenu(mesa);
                        break;
            }
        }while(op!=0 && (op>0 && op <= limite));
    }
    
    private void menuNovoJogo(Tabuleiro mesa)throws FileNotFoundException
    {
        int op=1;
        int limite;
        Scanner in = new Scanner(System.in);
        do{
            limite=2;
            System.out.println("----NOVO JOGO----");
            System.out.println("1 - Versus Player");
            System.out.println("2 - Versus PC");
            System.out.println("0 - Voltar");
            do{
                try{
                    System.out.print("Informe a opção: ");
                    String sop = in.nextLine();
                    op = Integer.parseInt(sop);
                    if(op<0 || op>limite){System.out.println("Digite um valor entre 0 e "+ limite +"!");op=-1;}
                }catch(Exception erro){
                    System.out.println("Digite um valor válido!");
                    op=-1;
                }
            }while(op<0 || op>2);
            switch(op)
            {
                case 1: int pl;
                        limite = 4;
                            System.out.println("--Versus Player--");
                            System.out.println("0 - Voltar");
                            System.out.println("Número de jogadores (2,3 ou 4): ");
                            do{
                            try{
                                System.out.print("Informe a opção: ");
                                String sop = in.nextLine();
                                pl = Integer.parseInt(sop);
                                if((pl<0 || pl>limite) || pl== 1){System.out.println("Digite um valor entre 2 e "+ limite +", ou 0 para voltar!");pl=-1;}
                            }catch(Exception erro){
                                System.out.println("Digite um valor válido!");
                                pl=-1;
                            }
                            if (pl == 0) break;
                        }while (pl<2 || pl>4);
                        if(pl!=0){
                        mesa.novoJogo(pl); // INICIAR O JOGO
                        limite=1;
                        System.out.println("---------" + pl + " Players---------");
                        System.out.println("-Colocar nomes específicos-");
                        System.out.println("0 - Não");
                        System.out.println("1 - Sim");
                        int op2;
                        do{
                            try{
                                System.out.print("Informe a opção: ");
                                String sop = in.nextLine();
                                op2 = Integer.parseInt(sop);
                                if(op2<0 || op2>limite){System.out.println("Digite um valor entre 0 e "+ limite +"!");op=-1;}
                            }catch(Exception erro){
                                System.out.println("Digite um valor válido!");
                                op2=-1;
                            }
                        }while (op2<0 || op2>1);
                        if(op2==1){
                            Scanner nomes = new Scanner(System.in);
                            int u=1;
                            String nome="";
                            do{
                                System.out.print("Digite o nome do player " + u +" (Vazio para Default): ");
                                try{
                                    nome = nomes.nextLine();
                                    if(nome.charAt(0)==' ');
                                }catch(Exception e){System.out.println("Nome Default");nome="Player "+u;}
                                mesa.NomePlayers(nome,u);
                                u++;
                            }while (u<=pl);
                        }
                        System.out.print("Iniciando jogo");
                        for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(800);
                                    }catch(Exception e){}
                                }
                                System.out.print("\f");
                                int Cm = mesa.ComecaDoble(1,pl);
                                menuPlayer(mesa,pl,Cm);
                        }
                    break;
                    
                case 2: mesa.novoJogo(1);
                        Scanner nomes = new Scanner(System.in);
                        String nome="Player 1";
                        System.out.print("Digite seu nome (Vazio para Default): ");
                        try{
                            nome = nomes.nextLine();
                            if(nome.charAt(0)==' ');
                        }catch(Exception e){System.out.println("Nome Default");nome="Player 1";}
                        mesa.NomePlayers(nome,1);
                        mesa.NomePlayers("PC",2); 
                        System.out.print("Iniciando jogo");
                        for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(1000);
                                    }catch(Exception e){}
                                }
                        System.out.print("\f");
                        int Cm = mesa.ComecaDoble(1,1);
                        if(Cm==2)jogarPC(mesa,0);
                        else menuPlayer(mesa,1,1);
                        
                        
                    break;
                case 0: System.out.print("\f");
                    break;
            }
        }while(op!=0);
    }
    
    private void menuPlayer(Tabuleiro mesa,int players,int player)throws FileNotFoundException
    {
        Scanner in = new Scanner(System.in);
        int op= 1,limite,excecao,excecao2;
        do{
            excecao=-1;
            excecao2=-1;
            if(players!=1)System.out.print("\f");
            System.out.println(mesa.nomePlayer(player) + ": ");
            System.out.println(mesa);
            System.out.print("\n");
            int imp=players;
            if(players==1)imp=players+1;
            for(int io=1;io<=imp;io++)
            {
                if(io!=player)
                {
                    System.out.print(mesa.nomePlayer(io) + ": " + mesa.getMao(io) +" peça");
                    if(mesa.getMao(io)>1)System.out.print("s");
                    System.out.println(" ");
                }
            }
            System.out.print("\n");
            System.out.println(mesa.playerToString(player)); // PRINT MÃO USUARIO
            boolean semPeca = mesa.MazoVazio();
            System.out.print("\n");
            System.out.println("---" + mesa.nomePlayer(player) + "---");
            System.out.println("1 - Jogar");
            limite=1;
            if(mesa.jogadaAtual()>1){System.out.println("2 - Inverter Tabuleiro");limite=2;excecao=-1;}
            else excecao=2;
            if(!semPeca){System.out.println("3 - Comprar");limite=3;excecao2=-1;}
            else {
                System.out.println("4 - Passar a vez");limite=4; excecao2=3;
                if(mesa.jogadaAtual()>0)
                    {System.out.println("5 - Verificar jogo trancado");limite=5;}
            }
            System.out.println("0 - Sair do jogo");
            do{
                try{
                    System.out.print("Digite a opção: ");
                    String sop = in.nextLine();
                    op = Integer.parseInt(sop);
                    if(op<0 || op>limite)
                    {
                        System.out.print("Digite um valor entre 0 e "+ limite);
                        if(excecao  == 2 || excecao2 == 3) System.out.print(" com exceção da opção ");
                        if(excecao == 2) System.out.print("2");
                        if(excecao==2 && excecao2 == 3) System.out.print(" e da opção ");
                        if(excecao2 == 3) System.out.print("3");
                        if(excecao2 == 4) System.out.print("4");
                        System.out.print("\n");
                        op=-1;
                    }
                    else if((excecao != -1 && op==2) || (excecao2 != -1 && op==3))
                    {
                        System.out.print("Digite um valor entre 0 e "+ limite);
                        if(excecao  == 2 || excecao2 == 3) System.out.print(" com exceção da opção ");
                        if(excecao == 2) System.out.print("2");
                        if(excecao==2 && excecao2 == 3) System.out.print(" e da opção ");
                        if(excecao2 == 3) System.out.print("3");
                        if(excecao2 == 4) System.out.print("4");
                        System.out.print("\n");
                        op=-1;
                        excecao=-1;
                        excecao2=-1;
                    }
                }catch(Exception erro){
                    System.out.println("Digite um valor válido!");
                    op=-1;
                }
            }while(op<0 || op>limite);
            switch(op)
            {
                case 1: int posPeca=1;
                limite = mesa.getMao(player);
                        do{
                        try{
                            System.out.print("Escolha a peça que deseja jogar: ");
                            String pospeca = in.nextLine();
                            posPeca = Integer.parseInt(pospeca);
                            posPeca=posPeca-1;
                            if(posPeca>mesa.getMao(player)) {
                                System.out.print("Você só tem " + mesa.getMao(player) + " Peça");
                                if(mesa.getMao(player)>1) System.out.println("s");
                                else System.out.print("\n");
                            }
                            if(posPeca<0 || posPeca>limite)
                            {
                                if(mesa.getMao(player)>0)
                                {
                                    System.out.println("Digite um valor entre 1 e "+ mesa.getMao(player) +"!");posPeca=-1;
                                }
                                else if(mesa.getMao(player)==1)System.out.println("Você só pode digitar 1!");
                            }
                            if(mesa.jogadaAtual()==0)
                            {
                                if(mesa.ComecaDoble(3,players)!=posPeca){
                                    System.out.println("Jogue seu maior doble na primeira rodada!");
                                    posPeca=-1;
                                }
                            }
                        }catch(Exception erro){
                            System.out.println("Digite um valor válido!");
                            op=-1;
                        }
                    }while (posPeca<0 || posPeca>mesa.getMao(player));
                        boolean result = mesa.Jogar(player,posPeca); 
                        if(result) {
                            boolean ganhou = mesa.ganhouJogo(player);
                            if(ganhou) {
                                System.out.println(mesa.nomePlayer(player) + " GANHOU O JOGO!");
                                op=0;
                                System.out.print("Saindo do jogo");
                                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(1500);
                                    }catch(Exception e){}
                                }
                                System.out.print("\f");
                                mesa = new Tabuleiro();
                                appMenu(mesa);
                                break;
                            }
                            else{
                                System.out.println("'Peça" + (posPeca+1) + "' Jogada");
                                System.out.print("Passando sua vez");
                                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(800);
                                    }catch(Exception e){}
                                }
                                if(players == 1) {System.out.println("\fPC:");jogarPC(mesa,0);}
                                else if(player==players) 
                                {
                                    System.out.print("\f");
                                    System.out.println(mesa.nomePlayer(1) + ": ");
                                    System.out.print("Digite ENTER para continuar...");
                                    try{
                                        String temp = in.nextLine();
                                        System.out.print("\f");
                                    }catch(Exception erro){}
                                    menuPlayer(mesa,players,1);
                                }
                                else 
                                {
                                    System.out.print("\f");
                                    System.out.println(mesa.nomePlayer(player+1) + ": ");
                                    System.out.print("Digite ENTER para continuar...");
                                    try{
                                        String temp = in.nextLine();
                                        System.out.print("\f");
                                    }catch(Exception erro){}
                                    menuPlayer(mesa,players,player+1);
                                }
                            }
                        }
                        else System.out.print("Erro ao jogar a peça " + (posPeca+1));
                        for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(600);}catch(Exception e){}}
                                System.out.print("\f");
                       break;
                case 2: if(mesa.jogadaAtual()>1){
                            System.out.print("Invertendo");
                            for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(450);}catch(Exception e){}}
                            System.out.print("\f");
                        }
                        mesa.InverterTab();       
                    break;
                case 3: result = mesa.comprarPeca(player);
                        if(!result) System.out.println("Baralho sem peças!");
                        System.out.print("\f");
                        break;
                case 4:if(semPeca){
                            System.out.print("Passando sua vez");
                                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(800);
                                    }catch(Exception e){}
                                }
                                if(players == 1) {System.out.println("\fPC:");jogarPC(mesa,0);}
                                else if(player==players) 
                                {
                                    System.out.print("\f");
                                    System.out.println(mesa.nomePlayer(1) + ": ");
                                    System.out.print("Digite ENTER para continuar...");
                                    try{
                                        String temp = in.nextLine();
                                        System.out.print("\f");
                                    }catch(Exception erro){}
                                    menuPlayer(mesa,players,1);
                                }
                                else 
                                {
                                    System.out.print("\f");
                                    System.out.println(mesa.nomePlayer(player+1) + ": ");
                                    System.out.print("Digite ENTER para continuar...");
                                    try{
                                        String temp = in.nextLine();
                                        System.out.print("\f");
                                    }catch(Exception erro){}
                                    menuPlayer(mesa,players,player+1);
                                }
                       }
                    break;
                case 5: if(mesa.jogadaAtual()>0){
                            System.out.print("Verificando jogo trancado");
                            for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(450);}catch(Exception e){}}
                            System.out.print("\n");
                            boolean trancou = mesa.JogoTrancado(players);
                            if(trancou){
                                System.out.println("Jogo trancado!");
                                try{Thread.sleep(1200);}catch(Exception e){}
                                System.out.print("Verificando quem ganhou o jogo");
                                for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(700);}catch(Exception e){}}
                                System.out.print("\n");
                                int qmganhou = mesa.ContarPontos(1,players);
                                int pts=0;
                                if(qmganhou!=0){
                                    pts = mesa.ContarPontos(2,players);
                                    System.out.println(mesa.nomePlayer(qmganhou) + " Ganhou o jogo, com " + pts + " pontos!");
                                    System.out.print("Saindo do jogo");
                                    for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(1600);}catch(Exception e){}}
                                System.out.print("\f");
                                appMenu(mesa);
                                }
                                else {
                                    System.out.println("Jogo empatado! Players com os mesmos pontos na mão!");
                                    System.out.print("Saindo do jogo");
                                    for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(1600);}catch(Exception e){}}
                                    System.out.print("\f");
                                    appMenu(mesa);
                                    }
                            }
                            else {
                                System.out.println("Jogo não está trancado!");
                                try{Thread.sleep(2000);}catch(Exception e){}
                                System.out.print("\f");
                            }
                        }
                    break;
                    case 0: System.out.println("---SAIR DO JOGO---");
                            System.out.println("DESEJA SALVAR O PROGRESSO?");
                            System.out.println("0 - Não\n1 - Sim");
                            int opc=0;
                            do{
                                try{
                                    limite=1;
                                    System.out.print("Informe a opção: ");
                                    String sop = in.nextLine();
                                    opc = Integer.parseInt(sop);
                                    if(op<0 || op>limite){System.out.println("Digite um valor entre 0 e "+ limite +"!");opc=-1;}
                                }catch(Exception erro){
                                    System.out.println("Digite um valor válido!");
                                    opc=-1;
                                }
                            }while(opc>1 || opc <0);
                            //System.out.print("\f");
                            if(opc == 1){
                                System.out.println("----SLOTS DE MEMÓRIA----");
                                int opd=0;
                                try{
                                    File origem = new File("JogosSalvos.txt");
                                    Scanner arquivo = new Scanner(origem);
                                    for(int i=0;i<3;i++)System.out.println(arquivo.nextLine());
                                    arquivo.close();
                                }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
                                do{
                                    try{
                                        System.out.print("Digite qual dos Slots você deseja Salvar: ");
                                        String sop = in.nextLine();
                                        opd = Integer.parseInt(sop);
                                        if(opd<1 || opd>3){System.out.println("Digite um Slot válido!");opd=-1;}
                                    }catch(Exception erro){
                                        System.out.println("Digite um valor válido!");
                                        opd=-1;
                                    }
                                }while(opd==-1);
                                if(mesa.SalvoExiste(opd))System.out.print("Sobrescrevendo dados");
                                else System.out.print("Salvando dados");
                                    for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(1100);}catch(Exception e){}}
                                System.out.print("\f");
                                mesa.salvarJogo(player,opd);
                                appMenu(mesa);
                            }//SALVAR O PROGRESSO
                            else if(opc == 0)
                            {
                                System.out.print("Saindo do jogo");
                                for(int i=0;i<3;i++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(400);
                                    }catch(Exception e){}
                                }
                                System.out.print("\f");
                                appMenu(mesa); //SAIR SEM SALVAR
                            }
                            break;
            }
        }while(op!=0);
    }
    
    private void jogarPC(Tabuleiro mesa,int comprou)throws FileNotFoundException
    {
        //System.out.println(mesa.playerToString(2));
        System.out.print("\f");
        System.out.println("PC: ");
        System.out.println(mesa);
        try{Thread.sleep(1500);}catch(Exception e){}
        boolean semPeca = mesa.MazoVazio();
        boolean seComprou = false,seJogou=false,interruptor = true;
        int tentar=1,qt=0;
        do{
            do{
            if (tentar==2){mesa.InverterTab();}
            for(int i=0; i<mesa.getMao(2); i++)
            {
                boolean result=false;
                if(mesa.jogadaAtual()==0)
                {
                    int maior = mesa.ComecaDoble(3,1);
                    result=mesa.Jogar(2,maior);
                }
                else result = mesa.Jogar(2,i); 
                if(result) {
                    boolean ganhou = mesa.ganhouJogo(2);
                    if(ganhou) {
                        System.out.println("PC GANHOU O JOGO!");
                        System.out.print("Saindo do jogo");
                        for(int io=0;io<3;io++)
                                {
                                    try{
                                        System.out.print(".");
                                        Thread.sleep(1500);
                                    }catch(Exception e){}
                                }
                                System.out.print("\f");
                                mesa = new Tabuleiro();
                                appMenu(mesa);
                        interruptor = false;
                    }
                    else{
                        System.out.println("");
                        if(tentar==2){
                            System.out.println("PC INVERTEU O TABULEIRO");
                            try{Thread.sleep(1000);}catch(Exception e){}
                        }
                        System.out.println("\fPC: ");
                        System.out.println(mesa);
                        System.out.println("");
                        if(comprou>0){
                            System.out.print("PC COMPROU "+comprou+" VEZ");
                            if(comprou>1)System.out.println("ES");
                            else System.out.print("\n");
                        }
                        System.out.println("PC JOGOU");
                        seJogou=true;
                        try{Thread.sleep(1000);}catch(Exception e){}
                        System.out.print("Voltando");
                        for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(800);}catch(Exception e){}}
                                System.out.print("\f");
                        interruptor = false;
                        menuPlayer(mesa,1,1);
                    }
                }
            }
            tentar++;
        }while(tentar<=2);
        if(interruptor)
        {
                    mesa.InverterTab();
            boolean result = mesa.comprarPeca(2);
                    if(result)
                    {
                        comprou++;
                    }
                    else
                    {
                        if(semPeca)
                        {
                            System.out.println("PC PASSOU A VEZ");
                            try{Thread.sleep(400);}catch(Exception e){}
                            System.out.print("Voltando");
                            for(int io=0;io<3;io++){try{System.out.print(".");Thread.sleep(500);}catch(Exception e){}}
                            System.out.print("\f");
                            menuPlayer(mesa,1,1);
                        }
                    }
                    jogarPC(mesa,comprou);
                    
        }
       }while(seJogou==false);
    }
}