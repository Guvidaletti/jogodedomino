import java.util.Random;
import java.io.File; //Necessário para trabalhar com arquivos (File)
import java.io.FileNotFoundException; //Necessário em razão da excessão gerada quando o arquivo não é encontrado.
import java.io.PrintWriter; //Necessário para gravar em arquivo
import java.util.Scanner; //Necesário para leitura de arquivos
/**
 * Escreva a descrição da classe Tabuleiro aqui.
 * 
 * @author Gustavo Lodi Vidaletti 
 * @version 2.0
 */
public class Tabuleiro
{
    private Peca[] Pedras,Jogadas;
    private int[] espelho;
    private int pedras,posicao2,jogadores,playerAoSalvar; //necessario pra carregar o jogo
    private Player p1,p2,p3,p4;
    private Baralho Mazo;
    public Tabuleiro()
    {
        pedras=28;
        Pedras = new Peca[pedras];
        Jogadas = new Peca[28];
    }
    
    public void novoJogo(int jogadores)
    {
        this.jogadores = jogadores;
        p1 = new Player(1,"Player 1");
        p2 = new Player(2,"Player 2");
        p3 = new Player(3,"Player 3");
        p4 = new Player(4,"Player 4");
        if (jogadores == 1) p2 = new Player(2, "PC");
        Mazo = new Baralho();
        pedras=0;
        posicao2=0;
        CriarPecas();
        sortearMaos();
    }
    
    /**
    * Método para Adicionar peças
    */
    private void addPeca(int i, int j)
    {
        Peca p = new Peca(i,j);
        Pedras[pedras] = p;
        pedras++;
    }
    
    /**
     * Cria as peças de um jogo
     * de dominó
     */
    private void CriarPecas()
    {
        if (pedras < 28)
        {
            for (int i=0 ; i <= 6 ; i++)
            {
                for (int j = i ; j <= 6 ; j++)
                {
                    addPeca(i,j);
                }
            }
        }
    }
    
    private Player buscaPlayer(int pl)
    {
        if(pl==1) return p1;
        if(pl==2) return p2;
        if(pl==3) return p3;
        if(pl==4) return p4;
        return null;
    }
    
    public boolean MazoVazio()
    {
        return Mazo.Vazio();
    }
    
    public void NomePlayers(String nome,int pl)
    {
        Player pp = buscaPlayer(pl);
        pp.setNome(nome);
    }
    
    public String nomePlayer(int pl)
    {
        Player pp = buscaPlayer(pl);
        return pp.getNome();
    }

    public int getMao(int pl)
    {
        Player pp = buscaPlayer(pl);
        return pp.getMao();
    }
    
    public String playerToString(int pl)
    {
        Player pp = buscaPlayer(pl);
        return pp.toString();
    }
    
    /**
     * Sorteia as mãos dos Players 
     * e o que vai para o baralho
     */
        private void sortearMaos()
    {   
        Random aleatorio = new Random();
        int on = jogadores;
        if(jogadores==1) on++;
        Peca aPeca;
        for(int r=1;r<=on;r++)
        {
            Player px = buscaPlayer(r);
            for(int j=0; j<7; j++)
            {
                int Rand = aleatorio.nextInt(pedras);
                aPeca = Pedras[Rand];
                px.addPeca(aPeca);
                excluir(0,Rand);
            }
        }
        if(on<=3){
            for(int i=0;i<14;i++)
            {
                aPeca = Pedras[i];
                Mazo.addPeca(aPeca);
                if (pedras==0) break;
            }
        }
    }
    
    /**
     * pl=0=Pedras
     */
    private void excluir (int pl,int numero)
    {
        if(pl==0)
        {
            for(int i=numero;i<pedras-1;i++)
            {
                Pedras[i] = Pedras[i+1];
            }
            pedras--;
        }
    }
    
    /**
     * a pedidos
     * Método para inverter o vetor e poder colocar no outro lado
     * também
     */
    public void InverterTab()
    {
        int j=posicao2,lim=posicao2/2;
        Peca laranja;
        j--;
        for(int i=0;i < lim; i++)
        {
            laranja = Jogadas[i];
            Jogadas[i] = Jogadas[j];
            Jogadas[j] = laranja;
            j--;
        }
        for(int i=0; i<posicao2; i++)
        {
            Jogadas[i].inverterPeca();
        }
    }
    
    public int ComecaDoble(int caso, int players)
    {
        int pl=0,MaiorDoble=0,Pmao=0,PedAt=0,numPeca=0;
        int jogando = players;
        if(players==1)jogando=2;
        for (int i=1; i<=jogando;i++)
        {
            Player px = buscaPlayer(i);
            Pmao = px.getMao();
            for(int j=0;j<Pmao;j++)
            {
                Peca aPeca = px.buscarPeca(j);
                boolean doble = aPeca.ConfereDoble();
                if(doble)
                {
                    PedAt = aPeca.getL1();
                    if(PedAt >= MaiorDoble){
                        MaiorDoble=PedAt;
                        pl=i;
                        numPeca=j;
                    }
                }
            }
        }
        if(caso==1)return pl;
        if(caso==2)return MaiorDoble;
        if(caso==3)return numPeca;
        return -1;
    }
       
    /**
     * Verifica se o jogo trancou ou não
     */
    public boolean JogoTrancado(int players)
    {
        int jogando=players,Pmao=0;
        boolean igual = false, MazoVazio = Mazo.Vazio();
        if (players==1)jogando++;
        if(MazoVazio){    
            for (int i=1; i<=jogando;i++)
            {
                Player px = buscaPlayer(i);
                Pmao = px.getMao();
                for(int j=0;j<Pmao;j++)
                    {
                    Peca aPeca = px.buscarPeca(j);
                    int l1 = aPeca.getL1();
                    int l2 = aPeca.getL2();
                    for(int k=1;k<=2;k++)
                    {
                        if(k==2)InverterTab();
                        if (l1 == ladoAtual() || l2 == ladoAtual())return false;
                    }
                }
            }
        }   
        return true;
    }
    
    /**
     * Se o jogo estiver trancado, conta os pontos de cada player
     * caso=1 Retorna o player que ganhou o jogo
     * caso=2 Retorna os pontos de cada player
     */
    public int ContarPontos(int caso, int players)
    {
        int jogando=players,Somap1,Somap2,Somap3,Somap4;
        int menorSoma=0,qmGanhou=0;
        if (players==1)jogando++;   
        Somap1=p1.SomarPontos();
        Somap2=p2.SomarPontos();
        Somap3=p3.SomarPontos();
        Somap4=p4.SomarPontos();
        qmGanhou=1;
        menorSoma=Somap1;
        if(Somap2==menorSoma)qmGanhou=0;
        else if(Somap2<menorSoma){qmGanhou=2;menorSoma=Somap2;}
        if(jogando>=3){
            if(Somap3==menorSoma){qmGanhou=0;}
            else if(Somap3<menorSoma){qmGanhou=3;menorSoma=Somap3;}
                }
        if(jogando==4){
            if(Somap4==menorSoma){qmGanhou=0;}
            else if(Somap4<menorSoma){qmGanhou=4;menorSoma=Somap4;}
            }
        if(caso==1)return qmGanhou;
        else if(caso==2) return menorSoma;
        else return 90;
    }
    
    public boolean Jogar(int pl, int num)
    {
        Peca aPeca;
        Player pp=buscaPlayer(pl);
        aPeca = pp.buscarPeca(num);
        if(posicao2==0)
          { 
              if(aPeca.ConfereDoble())
              {
                  Jogadas[posicao2] = aPeca;
                  posicao2++;
                  pp.excluir(num);
                  return true;
                        }
              }
              else 
              {
                  int lad2 = ladoAtual();
                  if(aPeca.getL1() == lad2)
                    {
                     Jogadas[posicao2] = aPeca;
                     posicao2++;
                     pp.excluir(num);
                     return true;
                        }
                  else if(aPeca.getL2() == lad2)
                     {
                      aPeca.inverterPeca();
                      Jogadas[posicao2] = aPeca;
                      posicao2++;
                      pp.excluir(num);
                      return true;
                        }
                    }
        return false;
    }
    
    /**
     * Método para verificar o lado da Pedra a ser verificado
     * Para colocar uma nova peça
     */
    private int ladoAtual()
    {
        Peca aAtual = Jogadas[posicao2-1];
        return aAtual.getL2();
    }
    
    /**
     * Método para verificar a jogada atual
     */
    public int jogadaAtual() {return posicao2;}
    /**
     * método para verificar o playerAtual
     */
    public int playerAtual() {return playerAoSalvar;}
    /**
     * método para verificar a quantidade de jogadores atual
     */
    public int playersAtual(){return jogadores;}
    
    public boolean comprarPeca(int pl)
    {
        Player px = buscaPlayer(pl);
        int num;
        Peca aPeca;
        if(Mazo.Vazio())return false;
        else
        {
            aPeca = Mazo.Comprar();
            px.addPeca(aPeca);
            return true;
        }
    }
    
    /**
     * Verifica se o Player ganhou o jogo
     */
    public boolean ganhouJogo(int pl)
    {
        Player pp = buscaPlayer(pl);
        if (pp.getMao()==0) return true;
        return false;
    }

    public String toString()
    {
        String msg= "Tabuleiro: \n";
        if (posicao2 == 0)  msg = msg + "Nenhuma peça jogada ainda. Escolha um Doble para iniciar o jogo.";
        for (int i=0 ; i < posicao2 ; i++)
        {
            msg = msg + Jogadas[i] + "" ;
        }
        msg = msg + "\n\nBaralho: " + Mazo.getTamanho() +" peça";
        if(Mazo.getTamanho()!=1) msg = msg+"s";
        return msg;
    }
    
        public void salvarJogo(int playeratual,int op)//throws FileNotFoundException 
    {
       try{
       PrintWriter arquivo;// = new PrintWriter("JogoSalvo.txt");
       if(op==1) arquivo = new PrintWriter("JogoSalvo1.txt");
       else if (op==2) arquivo = new PrintWriter("JogoSalvo2.txt");
       else arquivo = new PrintWriter("JogoSalvo3.txt");
       
       //PARA OS SLOTS
       Player ps = buscaPlayer(playeratual);       
       String Status = op + " - " + ps.getNome();
       if(jogadores>1)Status = Status + " (MultiPlayer),";
       else if(jogadores==1) Status = Status + " (SinglePlayer),";
       Status = Status + "(Player " + playeratual + " de ";
       if(jogadores==1) Status = Status + "2),(";
       else Status = Status + jogadores + "),(";
       if(posicao2==0) Status = Status + "Nenhuma peça jogada).";
       else if(posicao2==1) Status = Status + "Uma peça jogada).";
       else Status = Status + posicao2 + " Peças jogadas).";
       SalvarSlots((op-1),Status);
       
       //SAVE:
       arquivo.println(p1.getNome());
       arquivo.println(p2.getNome());
       arquivo.println(p3.getNome());
       arquivo.println(p4.getNome());
       arquivo.println(playeratual);
       arquivo.println(jogadores);
       arquivo.println(p1.getMao());
       arquivo.println(p2.getMao());
       arquivo.println(p3.getMao());
       arquivo.println(p4.getMao());
       arquivo.println(Mazo.getTamanho());
       arquivo.println(posicao2);
       Peca aPeca;
       Player pp;
       for(int pll=1;pll<=4;pll++)
       {
           pp = buscaPlayer(pll);
           for(int i=0; i<pp.getMao();i++)
           {
               aPeca = pp.buscarPeca(i);
               arquivo.println(aPeca.getL1());
               arquivo.println(aPeca.getL2());
            }
        }
       for(int i=0;i<Mazo.getTamanho();i++)
       {
           aPeca = Mazo.buscarPeca(i);
           arquivo.println(aPeca.getL1());
           arquivo.println(aPeca.getL2());
       }
       for (int i=0; i<posicao2;i++)
       {
           arquivo.println(Jogadas[i].getL1());
           arquivo.println(Jogadas[i].getL2());
       }
       arquivo.close();
      }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
    }
   
    public boolean SalvoExiste(int op)
    {
       File origem = new File("JogoSalvo1.txt");
       File origem2 = new File("JogoSalvo2.txt");
       File origem3 = new File("JogoSalvo3.txt");
       boolean Algum=false;
       if(origem.exists() || origem2.exists() || origem3.exists()) Algum=true;
       if (!Algum) CriarSlots();
       if (op==1) return origem.exists();
       else if(op==2) return origem2.exists();
       else if(op==3) return origem3.exists();
       else return Algum;
    }
    
    public void DeletarSlotSalvo(int op)
    {
        String nome = "";
        if(op==1) nome = "JogoSalvo1.txt";
        else if(op==2) nome = "JogoSalvo2.txt";
        else if(op==3) nome = "JogoSalvo3.txt";
        File origem = new File(nome);
        origem.delete();
        String Status = op + " - SLOT VAZIO";
        SalvarSlots((op-1),Status);
    }
    
    private void CriarSlots()
    {
        try{
            PrintWriter arquivo = new PrintWriter("JogosSalvos.txt");
            String msg = "1 - SLOT VAZIO\n2 - SLOT VAZIO\n3 - SLOT VAZIO";
            arquivo.println(msg);
            arquivo.close();
        }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
    }
    
    private void SalvarSlots(int op,String Status)
    {
        try{
            String[] Salvos = new String[3];
            File origem = new File("JogosSalvos.txt");
            Scanner arquivo = new Scanner(origem);
            Salvos[0]=arquivo.nextLine();
            Salvos[1]=arquivo.nextLine();
            Salvos[2]=arquivo.nextLine();
            arquivo.close();
            PrintWriter arq = new PrintWriter("JogosSalvos.txt");
            Salvos[op] = Status;
            arq.println(Salvos[0]);
            arq.println(Salvos[1]);
            arq.println(Salvos[2]);
            arq.close();
        }catch(FileNotFoundException e){System.out.println("Arquivo não encontrado");}
    }
    
    public void carregarJogo(int op)
    {
        try{
            String nome = "";
            if(op==1) nome = "JogoSalvo1.txt";
            else if(op==2) nome = "JogoSalvo2.txt";
            else if(op==3) nome = "JogoSalvo3.txt";
            File origem = new File(nome);
            Scanner arquivo = new Scanner(origem);
            //Definição novo jogo
            p1 = new Player(1,"Player 1");
            p2 = new Player(2,"Player 2");
            p3 = new Player(3,"Player 3");
            p4 = new Player(4,"Player 4");
            Mazo = new Baralho();
            //Para usar depois
            int pp1=0,pp2=0,pp3=0,pp4=0,pp5=0;
            //Carregamentos
            p1.setNome(arquivo.nextLine());
            p2.setNome(arquivo.nextLine());
            p3.setNome(arquivo.nextLine());
            p4.setNome(arquivo.nextLine());
            playerAoSalvar = arquivo.nextInt();
            jogadores = arquivo.nextInt();
            pp1=arquivo.nextInt();
            pp2=arquivo.nextInt();
            pp3=arquivo.nextInt();
            pp4=arquivo.nextInt();
            pp5=arquivo.nextInt();
            posicao2=arquivo.nextInt();
            pedras=0;
            Player pp;
            for(int p=1; p<=4;p++)
            {
                pp = buscaPlayer(p);
                int po=0;
                if(p==1)po=pp1;
                else if(p==2)po=pp2;
                else if(p==3)po=pp3;
                else if(p==4)po=pp4;
                for(int i=0;i<po;i++)
                {
                    int l1, l2;
                    l1 = arquivo.nextInt();
                    l2 = arquivo.nextInt();
                    Peca aPeca = new Peca(l1,l2);
                    pp.addPeca(aPeca);
                }
            }
            
            for(int i=0; i<pp5;i++)
            {
                int l1, l2;
                    l1 = arquivo.nextInt();
                    l2 = arquivo.nextInt();
                Peca aPeca = new Peca(l1,l2); 
                Mazo.addPeca(aPeca);
            }
            int somatorio = pp1+pp2+pp3+pp4+pp5;
             for (int i=0; i<posicao2;i++)
             {
                 int l1, l2;
                 l1 = arquivo.nextInt();
                 l2 = arquivo.nextInt();
                 Peca aPeca = new Peca(l1,l2);
                 Jogadas[i]=aPeca;
            }

            arquivo.close();
        }catch(FileNotFoundException e) {System.exit(0);}
    }
    
}